---
layout: job_family_page
title: "Test JF"
---

Brief paragraph describing the role goes here. 

## Responsibilities 
- include these in bullet point formatting 
- like this 

## Requirements 
- also include these in bullet point formatting 

## Hiring Process 
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team/).
- include the interviews here along with length of time they should expect to spend with the interviewer 
Additional details about our process can be found on our [hiring page](/handbook/hiring).

## Relevant Links 
- [Marketing Handbook](/handbook/marketing)
- You can link to other relevant handbook links in this bulleted list as well 
